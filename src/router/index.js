import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/views/index'
import Home from '@/views/home'
import Category from '@/views/category'
import Cart from '@/views/cart'
import MyInfo from '@/views/myInfo'
import Error from '@/views/404.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: '',
      component: Index,
      children: [
        {
          path: '/',
          name: '首页',
          component: Home
        },
        {
          path: '/category',
          name: '分类',
          component: Category
        },
        {
          path: '/cart',
          name: '购物车',
          component: Cart
        },
        {
          path: '/myinfo',
          name: '我的',
          component: MyInfo
        },
      ]
    },
    {
      path: '/error',
      name: 'error',
      component: Error
    },
    {
      path: '*',
      name: 'error',
      redirect: { path: '/error'}
    }
  ]
})
