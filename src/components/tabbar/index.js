import tabbar from './tabbar.vue';
import tabbarItem from './tabbarItem.vue';

export {
    tabbar,
    tabbarItem
};